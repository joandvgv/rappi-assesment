const expect = require("chai").expect;
const Constraint = require("../src/constraint");
const constraint = new Constraint(101);

const validSize = constraint.validSize();
const validUpdate = constraint.validUpdate([1,1,1],Math.pow(10,10));

describe("Valid size", () => {
  describe("validSize()", ()=> {
    it("Should return false when value is greather than 100", ()=> {
      expect(validSize).to.equal(false);
    });
  });
  describe("validUpdate()", ()=> {
    it("Should return false when value is greather than 10^9", ()=> {
      expect(validUpdate).to.equal(false);
    });
  });
});

