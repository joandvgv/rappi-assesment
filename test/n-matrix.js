const expect = require("chai").expect;
const NMatrix = require("../src/n-matrix");
const matrix = new NMatrix(4,3);



describe("Query", () => {
  describe("query1()", function() {
    matrix.update([2,2,2],4);
    it("Should return 4 for query UPDATE 2 2 2 4 | QUERY 1 1 1 3 3 3", () => {
      expect(matrix.query([1,1,1],[3,3,3])).to.equal(4);
    });
  });
});