# README #

Rappi assesment (cube-summation problem)

### Getting started ###

* npm install
* npm start
* server will be running at localhost:3000
* GET / for testing the server
* POST / sending the input as the body of the request. i.e {"input": "1\n 4 5\nUPDATE 2 2 2 4\nQUERY 1 1 1 3 3 3"}
* You can import the example request to postman: https://www.getpostman.com/collections/00bbb03c30072b47e4f5

### Test ###

*  npm test

### Documentation ###

*  The application consist of an application layer (where the logic resides), there's no storage nor presentation layer (although data is presented in JSON Format) 
*  The src folder contains 4 classes: 
*  Class Router which manages the logic behind every request. 
*  Class Processor which parses input data as presented in hackerrank to perform the operations requested.
*  Class Constraint, which handles every validation required by hackerrank problem constraints. 
*  Class NMatrix which solves the problem by creating a N*N*N Array representation inside a single-dimension array, updates the values and iterates over specific indexes to calculate the sum of the values.  