const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const Router = require("./src/router")

class App {
  constructor(){

    const app = express();
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(cors());

    this.port = process.env.PORT || 3000;
    this.app = app;
  }

  start(){
    const port = this.port;
    const app = this.app;
    
    app.listen(port, err => {
      if (err) return console.error(err);
      console.log('Running a NMatrix Solver server at port 3000');
    })
  }

  bind(){
    const app = this.app;
    const routes = new Router();
    app.post('/', routes.process.bind(routes));
    app.get('/', routes.hello.bind(routes));
  }
}

const app = new App();
app.bind();
app.start();
