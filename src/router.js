const Processor = require("./processor");

class Router {
 
  process(req, res) {
    
    const processor = new Processor();
    const input = req.body.input;
    const hrstart = process.hrtime();
    const response = processor.processData(input);
    const hrend = process.hrtime(hrstart);
    
    res.json({
      result: response,
      time: hrend
    });
  }

  hello(req, res) {
      res.send("N Matrix");  
  }

}

module.exports = Router
