"use strict";

const Constraint = require("./constraint");

class NMatrix {
  constructor(size, dimension) {
    this.constraint = new Constraint(size);
    if (!this.constraint.validSize()) throw new Error("Tamaño inválido");

    this.matrix = [];
    this.size = size;
    this.dimension = dimension;

    const n = Math.pow(size, dimension);
    for (let i = 0; i < n; i++) {
      this.matrix.push(0);
    }
  }

  getIndex(params) {
    let index = 0,
      dimension = this.dimension;

    for (var i = 0; i < params.length; i++)
      index += (params[i] - 1) * Math.pow(this.size, dimension - (i + 1));
    return index;
  }

  update(params, w) {
    if (!this.constraint.validUpdate(params, w))
      throw new Error("Parámetros inválidos");

    let index = this.getIndex(params);

    this.matrix[index] = Number(w);
  }

  query(params1, params2) {
    if (!this.constraint.validQuery(params1, params2))
      throw new Error("Parámetros inválidos");

    let sum = 0;
    let start = this.getIndex(params1);
    let end = this.getIndex(params2);

    for (let i = start; i <= end; i++) sum += this.matrix[i];

    return sum;
  }
}

module.exports = NMatrix;
