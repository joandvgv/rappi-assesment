class Constraint {
  constructor(size) {
    this.size = size;
  }

  validSize() {
    return this.size >= 1 && this.size <= 100;
  }

  validUpdate(params, w) {
    let isValid = true;

    params.forEach(key => {
      isValid = !(key < 1 || key > this.size);
    });

    if (w < Math.pow(-10, 9) || w > Math.pow(10, 9)) {
      isValid = false;
    }
    return isValid;
  }

  validQuery(params1, params2) {
    let isValid = true;
    params1.forEach(key => {
      isValid = !(key < 1 || key > this.size);
    });

    params2.forEach(key => {
      isValid = !(key < 1 || key > this.size);
    });

    params2.forEach((key, i) => {
      isValid = params1[i] <= key;
    });
    return isValid;
  }
}

module.exports = Constraint;
