const NMatrix = require("./n-matrix");

class Processor{

  constructor(){
    this.operations = ["UPDATE", "QUERY"];
    this.matrix;
  }

  processData(input){
    const lines =  input.split("\n");
    const t = Number(lines[0]);
    let response = "";
    let i = 1,
      k = 0;
  
    while (t >= k) {
      if (!lines[i]) break;
      const firstValue = lines[i].trim().split(" ")[0]

      let parsed = this.parseInput(lines[i], firstValue)
      if(parsed) response += parsed
      if(parsed===false) k++;

      i++;
    }
    return response;
  }

  parseInput(line, firstValue){
    if (!isNaN(firstValue)) {
      const n = Number(firstValue);
      this.matrix = new NMatrix(n, 3);
      return false;
    }
    
    const result = this.perform(this.matrix, line.trim());
    if(result)
      return result + "\n";
  }

  perform(matrix,line){
    const operation = this.getOperation(line);
    if (line.includes("UPDATE"))
      return matrix.update(operation.splice(0,3), operation[0]);

    return matrix.query(
      operation.splice(0,3),
      operation,
    );
  }

  getOperation(line){
    const name = this.operations.find(element => {
      return line.includes(element);
    });
  
    return line
      .replace(`${name} `, "")
      .split(" ")
      .map(n => {
        return Number(n);
      });
  }
}

module.exports = Processor
